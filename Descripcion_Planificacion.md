
# Descripción y planificación detallada del proyecto #
 

»	Objetivo General: 
Análisis de medidas de centralidad de la red de transacciones entre clientes Bancomer
Detección de comunidades

Objetivos Opcionales:
Obtener el índice de exposición de un cliente, si la red se infecta (fraude) esa sería su probabilidad de resultar contagiado.

Los pasos a seguir son:
1.- Evaluar la calidad de la base de datos:
Modificar el formato o transformar variables si fuera necesario.
Detectar valores inválidos o missing y eliminar dichos registros.
Identificar la ventana de tiempo a estudiar y la población objetivo (por ejemplo, si quisiéramos evaluar solo a clientes Bancomer)

2.-  Definir la estructura de la red:
      ¿Cuáles son los nodos, vértices?
      ¿Cuál es la arquitectura de la red? Directed, undirected, weighted, multi-graph, etc.

3.-Analizar las posibilidades de GraphX para el análisis del grafo.

4.-Investigar cómo presentar y visualizar los resultados.

»	Fuentes de datos y herramientas. 
Nuestra fuente de datos será la base proporcionada para el reto graph y la herramienta que utilizaremos será Scala.


»	Planificación detallada de la primera iteración. 
1.- Exploración de la base de datos: 
     1.1 Cargar la base de datos en Scala
1.2 Explorar el contenido de las variables: los rangos entre los que se mueven los valores de las variables principales (Monto, Fecha de transacción, Hora de transacción, volumen de clientes, etc.)
1.3 Identificar y Eliminar registros inválidos o nulos.
     1.4 Guardar la base mini limpia para trabajar sobre ella.

2.- Realizar un diagrama en donde se muestre la estructura de la red: 
      los nodos, vértices, si es dirigida o no dirigida, con peso o no, etc.
 
 3.- Estudiar las principales funciones de GraphX y aplicarlas a una muestra de nuestra base. 

»	Objetivos de esta iteración. 
Familiarizarnos con la base de datos, generar el código para limpiarla y tenerla lista para trabajar con ella en Scala.
Identificar la red a estudiar, así como sus características. 
Familiarizarnos con la herramienta y ser capaces de implementar sus principales funciones. 

»	Fuentes de datos a emplear exclusivamente para esta iteración (tablas y campos). 
Base: Graph_mini
Campos: id, fec_transferencia, hora_transferencia, emisor, emisor_refundicion, emisor_ccliente, beneficiario, beneficiario_ccliente, importe, partition_id.

»	Calendario
Tareas 1 y 2.- Deadline: Viernes 3 Noviembre
Tarea 3.- Deadline: Viernes 10 Noviembre



